
public class Konsolenausgabe2 {

	public static void main(String[] args) {
		
		//Uebung 2
		//
		//Aufgabe 1
		System.out.printf("%4s", "**");
		System.out.println("");
		System.out.printf("%s  %3s", "*", "*");
		System.out.println("");
		System.out.printf("%s %4s", "*", "*");
		System.out.println("");
		System.out.printf("%4s", "**");
		System.out.println("");
		
		//Aufgabe 2
		int l = 0;
		int m = 1;
		int n = 2;
		int o = 3;
		int p = 4;
		int q = 5;
		String r = "!";
		String s = "=";
		String t = "*";
		System.out.printf("%s%s%5s%20s%4s", l, r, s, s, m);
		System.out.println("");
		System.out.printf("%s%s%5s%2s%18s%4s", m, r, s, m ,s, m);
		System.out.println("");
		System.out.printf("%s%s%5s%2s%2s%2s%14s%4s", n, r, s, m, t, n, s, n);
		System.out.println("");
		System.out.printf("%s%s%5s%2s%2s%2s%2s%2s%10s%4s", o, r, s, m, t, n, t, o, s, n*o);
		System.out.println("");
		System.out.printf("%s%s%5s%2s%2s%2s%2s%2s%2s%2s%6s%4s", p, r, s, m, t, n, t, o, t, p, s, n*o*p);
		System.out.println("");
		System.out.printf("%s%s%5s%2s%2s%2s%2s%2s%2s%2s%2s%2s%2s%4s", q, r, s, m, t, n, t, o, t, p, t, q, s, n*o*p*q);
		
		System.out.println("");
		//Aufgabe 3
		String a = "Fahrenheit";
		String b = "Celsius";
		int c = 0;
		int d = 10;
		int e = 20;
		int f = 30;
		double g = 28.8889;
		double h = 23.3333;
		double i = 17.7778;
		double j = 6.6667;
		double k = 1.1111;
		
		System.out.printf( "%-12s", a);
		System.out.printf( "|%9s", b);
		System.out.println("");
		System.out.println("----------------------");
		System.out.printf( "%-11s | %8.2f", -e, -g);
		System.out.println("");
		System.out.printf( "%-12s| %8.2f", -d, -h);
		System.out.println("");
		System.out.printf( "+%-10s | %8.2f", c, -i);
		System.out.println("");
		System.out.printf( "+%-10s | %8.2f", e, -j);
		System.out.println("");
		System.out.printf( "+%-10s | %8.2f", f, -k);

	}

}
