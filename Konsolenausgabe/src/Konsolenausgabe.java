
public class Konsolenausgabe {

	public static void main(String[] args) {
		
		String a = "Beispielsatz";
		System.out.println("Das ist ein " + a + ".");
		System.out.println("Ein " + a + " ist das.");
		
		// Das ist ein Kommentar.
		
		
		
		System.out.print("      * \n" + 
		                 "     *** \n" +
		                 "    ***** \n" + 
		                 "   ******* \n" +
		                 "  ********* \n" +
		                 " *********** \n" +
		                 "************* \n" +
		                 "     *** \n" + 
		                 "     *** \n");
		
		System.out.println("");
		
	    double b = 22.4234234;
		double c = 111.2222;
		double d = 4.0;
		double e = 1000000.55;
		double f = 97.34;
		
		System.out.printf("|%.2f|\n" , b);
		System.out.printf("|%.2f|\n" , c);
		System.out.printf("|%.2f|\n" , d);
		System.out.printf("|%.2f|\n" , e);
		System.out.printf("|%.2f|" , f);

	}

}
