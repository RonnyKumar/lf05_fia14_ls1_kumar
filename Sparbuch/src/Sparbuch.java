
public class Sparbuch {
	
	private int kontonummer;
	private double kapital;
	private double zinssatz;
	
	public Sparbuch (int kontonummer, double kapital, double zinssatz) {
		this.kontonummer = kontonummer;
		this.kapital = kapital;
		this.zinssatz = zinssatz;
	}
	
	public void setKontonummer (int kn) {
		this.kontonummer = kn;
	}
	
	public int getKontonummer () {
		return kontonummer;
	}
	
	public void setKapital (double kp) {
		this.kapital = kp;
	}
	
	public double getKapital () {
		return kapital;
	}
	
	public void setZinssatz (double zs) {
		this.zinssatz = zs;
	}
	
	public double getZinssatz () {
		return zinssatz;
	}
	
	public double zahleEin(double betrag) {
		this.kapital += betrag;
		return kapital;
	}
	
	public double hebeAb(double betrag) {
		double kap = this.kapital - betrag;
		if (kap < 0) {
			System.out.println("Kontostand: " + this.kapital + " . Auszahlung nicht m�glich.");
		} else {
			this.kapital = kap;
			System.out.println("Auszahlung: " + betrag);
		}
		return kapital;
	}
	

	
	public double verzinse() {
		double kap = this.kapital * this.zinssatz;
		this.kapital = kap;
		return kapital;
	}
	
}
