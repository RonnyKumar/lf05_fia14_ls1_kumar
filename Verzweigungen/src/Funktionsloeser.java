import java.util.Scanner;
import java.text.DecimalFormat;
public class Funktionsloeser {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		DecimalFormat df = new DecimalFormat("#0.00");
		System.out.print("Geben Sie einen x-Wert ein: ");
		double x = sc.nextDouble();
		double e = 2.718;
		double y;
		String ex = "exponentiell";
		String q = "quadratisch";
		String l = "linear";
		if (x <=0) {
			y = Math.pow(e, x);
			System.out.println("Funktionswert: " + df.format(y));
			System.out.print("Bereich: " + ex);
		} else {
			if (0 < x && x <= 3) {
				y = (x * x) + 1;
				System.out.println("Funktionswert: " + df.format(y));
				System.out.print("Bereich: " + q);
			} else {
				if (x > 3) {
					y = 2 * x + 4; 
					System.out.println("Funktionswert: " + df.format(y));
					System.out.print("Bereich: " + l);
				}
			}
		}

		sc.close();
	}

}
