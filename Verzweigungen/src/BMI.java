import java.util.Scanner;
import java.text.DecimalFormat;
public class BMI {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		DecimalFormat df = new DecimalFormat("#0.00");
		System.out.print("Geben Sie ihr Geschlcht ein (m/w): ");
		String geschlecht = sc.next();
		System.out.print("Geben Sie ihr Gewicht ein (kg): ");
		double gewicht = sc.nextDouble();
		System.out.print("Geben Sie ihre Gr��e ein (cm): ");
		double groe�e = sc.nextDouble();
		groe�e = groe�e /100;
		double bmi = gewicht / (groe�e * groe�e);
		System.out.println("Ihr BMI lautet: " + df.format(bmi));
		
		switch (geschlecht) {
		case "m":
			if (bmi < 20) {
				System.out.print("Untergewicht");
			} else {
				if (20 <= bmi && bmi <= 25) {
					System.out.print("Normalgewicht");
				} else {
					if (bmi > 25) {
						System.out.print("�bergewicht");
					}
				}
			}
			break;
		case "w":
			if (bmi < 19) {
				System.out.print("Untergewicht");
			} else {
				if (19 <= bmi && bmi <= 24) {
					System.out.print("Normalgewicht");
				} else {
					if (bmi > 24) {
						System.out.print("�bergewicht");
					}
				}
			}
			break;
		default:
			System.out.print("Geben Sie den richtigen Buchstaben ein (m/w)");
		}
		
		sc.close();
		
	}

}
