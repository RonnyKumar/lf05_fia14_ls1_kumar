import java.util.Scanner;
import java.text.DecimalFormat;
public class Grosshaendler {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		DecimalFormat df = new DecimalFormat("#0.00");
		System.out.print("Geben Sie die Anzahl der M�use in: ");
		int anzahl = sc.nextInt();
		System.out.print("Geben Sie den Preis f�r eine Maus ein: ");
		double nettopreis = sc.nextDouble();
		double bruttopreis = nettopreis * 1.19;
		double gesamtpreis = bruttopreis * anzahl;
		
		if (anzahl < 10) {
			if (gesamtpreis <= 100) {
				gesamtpreis = (gesamtpreis * 0.9) + 10;
				System.out.print("Der Rechnungsbetrag betr�gt: " + df.format(gesamtpreis) + " �");
			} else {
				if (100 < gesamtpreis && gesamtpreis <= 500) {
					gesamtpreis = (gesamtpreis * 0.85) + 10;
					System.out.print("Der Rechnungsbetrag betr�gt: " + df.format(gesamtpreis) + " �");
				} else {
					if (gesamtpreis > 500) {
						gesamtpreis = (gesamtpreis * 0.8) + 10;
						System.out.print("Der Rechnungsbetrag betr�gt: " + df.format(gesamtpreis) + " �");
					}
				}
			}
			
		} else {
			if (gesamtpreis <= 100) {
				gesamtpreis = gesamtpreis * 0.9;
				System.out.print("Der Rechnungsbetrag betr�gt: " + df.format(gesamtpreis) + " �");
			} else {
				if (100 < gesamtpreis && gesamtpreis <= 500) {
					gesamtpreis = gesamtpreis * 0.85;
					System.out.print("Der Rechnungsbetrag betr�gt: " + df.format(gesamtpreis) + " �");
				} else {
					if (gesamtpreis > 500) {
						gesamtpreis = gesamtpreis * 0.8;
						System.out.print("Der Rechnungsbetrag betr�gt: " + df.format(gesamtpreis) + " �");
					}
				}
			}
		}
		sc.close();
	}

}
