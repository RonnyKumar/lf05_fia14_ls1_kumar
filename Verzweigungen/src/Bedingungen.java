import java.util.Scanner;
public class Bedingungen {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Zahl 1: ");
		double a = sc.nextDouble();
		System.out.print("Zahl 2: ");
		double b = sc.nextDouble();
		System.out.print("Zahl 3: ");
		double c = sc.nextDouble();
		
		if (a == b) {
			System.out.println("Die Zahlen sind gleich");
		}
		
		if (b > a) {
			System.out.println("Zahl 2 ist gr��er als Zahl 1");
		}
		
		if (a >= b) {
			System.out.println("Zahl 1 ist gr��er oder gleich Zahl 2");
		} else {
			System.out.println("Zahl 2 ist gr��er als Zahl 1");
		}
		
		if (a > b && a > c) {
			System.out.println("Zahl 1 ist gr��er als Zahl 2 und Zahl 3");
		}
		
		if (c > b || c > a) {
			System.out.println("Zahl 3 ist gr��er als Zahl 1 oder Zahl 2");
		}
		
		if (a > b && a > c) {
			System.out.println(a);
		} else {
			if (b > a && b > c) {
				System.out.println(b);
			} else {
				System.out.println(c);
			}
		}
		
		sc.close();

	}

}
