import java.util.Scanner;
import java.text.DecimalFormat;
public class Steuersatz {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		DecimalFormat df = new DecimalFormat("#0.00");
		System.out.print("Eingabe Nettowert: ");
		double netto = sc.nextDouble();
		System.out.print("Geben Sie ein j f�r den erm��igten oder ein n f�r den vollen Steuersatz ein: ");
		String m = sc.next();
		
		switch (m) {
			case "j":
				netto = netto * 1.07;
				System.out.print("Der Bruttobetrag lautet: " + df.format(netto));
				break;
			case "n":
				netto = netto * 1.19;
				System.out.print("Der Bruttobetrag lautet: " + df.format(netto));
				break;
			default:
				System.out.print("Geben Sie den richtigen Buchstaben ein");
		}
		
		sc.close();
	}

}
