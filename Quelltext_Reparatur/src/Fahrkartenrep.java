
public class Fahrkartenrep {

	public static void main(String[] args) {
		
		String name;
		String typ = "Automat AVR";
		String bezeichnung = "Q2021_FAB_A";
		name = typ + " " + bezeichnung;
		
		char sprachModul = 'd';
		
		final byte PRUEFNR = 4;
		
		double prozent;
		double maximum = 100.00;
		double patrone = 46.24;
		prozent = maximum - patrone;
		
		int euro;
		int cent;
		int summe;
		int muenzenCent = 1280;
		int muenzenEuro = 130;
		summe = muenzenCent + muenzenEuro * 100;
		euro = summe / 100;
		cent = summe % 100;
		
		boolean status;
		status = (euro <= 150) 
		&& (euro >= 50)
		&& (cent != 0)
		&& (prozent >= 50.00) 
		&&  (!(PRUEFNR == 5 || PRUEFNR == 6))
		&& (sprachModul == 'd');
		System.out.println("Name: " + name);
		System.out.println("Sprache: " + sprachModul);
		System.out.println("Pr�fnummer : " + PRUEFNR);
		System.out.println("F�llstand Patrone: " + prozent + " %");
		System.out.println("Summe Euro: " + euro +  " Euro");
		System.out.println("Summe Rest: " + cent +  " Cent");		
		System.out.println("Status: " + status);

	}

}
