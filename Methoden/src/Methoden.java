import java.util.Scanner;
public class Methoden {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("x: ");
		double x = sc.nextDouble();
		System.out.print("y: ");
	    double y = sc.nextDouble();
	    double m = berechneMittelwert(x, y);
	    System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
	    
	    double z = 5;
	    double ergebnis = Quadrieren(z);
	    System.out.printf("z = %.2f und z�= %.2f\n", z, ergebnis);
	    
	    sc.close();
	}
	public static double berechneMittelwert(double x, double y) {
		return (x + y) / 2.0;
	}
	public static double Quadrieren(double z) {
		return z *z;
	}
}
