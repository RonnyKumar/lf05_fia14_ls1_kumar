import java.util.Scanner;
public class Mathe {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("x: ");
		double x = sc.nextDouble();
		double quadrate = quadrat(x);
		System.out.println("x^2: " + quadrate);
		
		double h = Mathe.hypothenuse(x, x);
		System.out.print("Hypothenuse: " + h);
		
		sc.close();
	}
	
	public static double quadrat(double x) {
		return x * x;
	}
	
	public static double hypothenuse(double kathete1, double kathete2) {
		return Math.sqrt(quadrat(kathete1) + quadrat(kathete2));
	}
	
}
