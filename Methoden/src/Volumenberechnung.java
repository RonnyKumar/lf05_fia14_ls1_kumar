import java.util.Scanner;
public class Volumenberechnung {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("a: ");
		double a = myScanner.nextDouble();
		System.out.print("b: ");
		double b = myScanner.nextDouble();
		System.out.print("c: ");
		double c = myScanner.nextDouble();
		System.out.print("h: ");
		double h = myScanner.nextDouble();
		System.out.print("r: ");
		double r = myScanner.nextDouble();
		
		double vw = Wuerfel(a);
		double vq = Quader(a, b, c);
		double vp = Pyramide(a, h);
		double vk = Kugel(r);
		
		System.out.println("Volumen W�rfel: " + vw);
		System.out.println("Volumen Quader: " + vq);
		System.out.println("Volumen Pyramide: " + vp);
		System.out.println("Volumen Kugel: " + vk);

		myScanner.close();
	}
	
	public static double Wuerfel(double a) {
		return a * a * a;
	}
	
	public static double Quader(double a, double b, double c) {
		return a * b * c;
	}
	
	public static double Pyramide(double a, double h) {
		return a * a * h/3;
	}
	
	public static double Kugel(double r) {
		return 4/3 * (r*r*r) * Math.PI;
	}

}
