import java.util.Scanner;
public class Schaltungsquadrat {
	
	public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);
	
	System.out.print("r1: ");
	double r1 = sc.nextDouble();
	System.out.print("r2: ");
	double r2 = sc.nextDouble();
	double reihe = reihenschaltung(r1, r2);
	double parallel = parallelschaltung(r1, r2);
	System.out.println("Ersatzwiderstand: " + reihe);
	System.out.print("Ersatzwiderstand: " + parallel);
	
	sc.close();
	}

	public static double reihenschaltung(double r1, double r2) {
		return r1 + r2;
	}
	
	public static double parallelschaltung(double r1, double r2) {
		return (r1 * r2)/(r1 + r2);
	}
	
}
