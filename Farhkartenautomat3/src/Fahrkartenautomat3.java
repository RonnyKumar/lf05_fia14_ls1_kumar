import java.util.Scanner;
import java.text.DecimalFormat;

public class Fahrkartenautomat3 {

	public static void main(String[] args) {
		
		DecimalFormat df = new DecimalFormat("#0.00");
        double zuZahlenderBetrag;
        double eingezahlterGesamtbetrag;
        zuZahlenderBetrag = fahrkartenbestellungErfassen();
        eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
        fahrkartenAusgeben();
        double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        rueckgeldAusgeben(rueckgabebetrag);

	}
	
	// Geldeinwurf
    // -----------
    public static double fahrkartenbestellungErfassen() {
        double zuZahlenderBetrag;
        double ticketPreis;
        int anzahlTickets;
        Scanner sc = new Scanner(System.in);
        System.out.print("Ticketpreis: ");
        ticketPreis = sc.nextDouble();
        System.out.print("Ticketanzahl: ");
        anzahlTickets = sc.nextInt();
        while (anzahlTickets <= 0 || anzahlTickets > 10) {
            System.out.println("Ung�ltige Anzahl.");
            anzahlTickets = sc.nextInt();
        }
        zuZahlenderBetrag = ticketPreis * anzahlTickets;
        sc.close();
        return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        Scanner sc = new Scanner(System.in);
        double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMuenze;

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format("Noch zu zahlen: %4.2f �%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
            eingeworfeneMuenze = sc.nextDouble();
            if (eingeworfeneMuenze == 0.05){
                eingezahlterGesamtbetrag += eingeworfeneMuenze;
            } else if (eingeworfeneMuenze == 0.1){
                eingezahlterGesamtbetrag += eingeworfeneMuenze;
            }  else if (eingeworfeneMuenze == 0.2){
                eingezahlterGesamtbetrag += eingeworfeneMuenze;
            }  else if (eingeworfeneMuenze == 0.5){
                eingezahlterGesamtbetrag += eingeworfeneMuenze;
            }  else if (eingeworfeneMuenze == 1){
                eingezahlterGesamtbetrag += eingeworfeneMuenze;
            } else if (eingeworfeneMuenze == 2){
                eingezahlterGesamtbetrag += eingeworfeneMuenze;
            } else {
                System.out.println("Nicht akzeptiertes Zahlungsmittel.");
            }

        }
        sc.close();
        return eingezahlterGesamtbetrag;
    }
    
    // Fahrscheinausgabe
    // -----------------
    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    // R�ckgeldberechnung und -Ausgabe
    // -------------------------------
    public static void rueckgeldAusgeben(double rueckgabebetrag) {
        if (rueckgabebetrag > 0.0) {
            System.out.format("Der R�ckgabebetrag in H�he von %4.2f � %n", rueckgabebetrag);
            System.out.println("wird in folgenden M�nzen ausgezahlt: ");
            while (rueckgabebetrag >= 2.0)
            {
                System.out.println("2 Euro");
                rueckgabebetrag -= 2.0;
                rueckgabebetrag = Math.round(rueckgabebetrag*100.0)/100.0;
            }
            while (rueckgabebetrag >= 1.0)
            {
                System.out.println("1 Euro");
                rueckgabebetrag -= 1.0;
                rueckgabebetrag = Math.round(rueckgabebetrag*100.0)/100.0;
            }
            while (rueckgabebetrag >= 0.5)
            {
                System.out.println("50 Cent");
                rueckgabebetrag -= 0.5;
                rueckgabebetrag = Math.round(rueckgabebetrag*100.0)/100.0;
            }
            while (rueckgabebetrag >= 0.2)
            {
                System.out.println("20 Cent");
                rueckgabebetrag -= 0.2;
                rueckgabebetrag = Math.round(rueckgabebetrag*100.0)/100.0;
            }
            while (rueckgabebetrag >= 0.1)
            {
                System.out.println("10 Cent");
                rueckgabebetrag -= 0.1;
                rueckgabebetrag = Math.round(rueckgabebetrag*100.0)/100.0;
            }
            while (rueckgabebetrag >= 0.05)
            {
                System.out.println("5 Cent");
                rueckgabebetrag -= 0.05;
                rueckgabebetrag = Math.round(rueckgabebetrag*100.0)/100.0;
            }
        }
    }

}
