import java.util.Scanner;
public class Noten {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Geben Sie eine Note ein (1-6): ");
		int note = sc.nextInt();
		
		switch (note) {
		case 1:
			System.out.print("Sehr gut");
			break;
		case 2:
			System.out.print("Gut");
			break;
		case 3:
			System.out.print("Befriedigend");
			break;
		case 4:
			System.out.print("Ausreichen");
			break;
		case 5:
			System.out.print("Mangelhaft");
			break;
		case 6:
			System.out.print("Ungen�gend");
			break;
		default:
			System.out.print("Geben Sie eine g�ltige Zahl von 1-6 ein.");
		}
		sc.close();
	}

}
