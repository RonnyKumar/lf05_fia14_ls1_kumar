import java.util.Scanner;
public class Rom {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Geben Sie ein Zeichen ein (I,V,X,L,C,D,M): ");
		String zeichen = sc.next();
		switch (zeichen) {
		case "I":
			System.out.print("1");
			break;
		case "V":
			System.out.print("5");
			break;
		case "X":
			System.out.print("10");
			break;
		case "L":
			System.out.print("50");
			break;
		case "C":
			System.out.print("100");
			break;
		case "D":
			System.out.print("500");
			break;
		case "M":
			System.out.print("1000");
			break;
		default:
			System.out.print("Geben Sie ein g�ltiges Zeichen ein (I,V,X,L,C,D,M).");
		}
		sc.close();
	}

}
