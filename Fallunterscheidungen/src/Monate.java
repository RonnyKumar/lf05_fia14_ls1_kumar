import java.util.Scanner;
public class Monate {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Geben Sie ein Zahl ein (1-12): ");
		int zahl = sc.nextInt();
		
		switch (zahl) {
		case 1:
			System.out.print("Januar");
			break;
		case 2:
			System.out.print("Februar");
			break;
		case 3:
			System.out.print("M�rz");
			break;
		case 4:
			System.out.print("April");
			break;
		case 5:
			System.out.print("Mai");
			break;
		case 6:
			System.out.print("Juni");
			break;
		case 7:
			System.out.print("Juli");
			break;
		case 8:
			System.out.print("August");
			break;
		case 9:
			System.out.print("September");
			break;
		case 10:
			System.out.print("Oktober");
			break;
		case 11:
			System.out.print("November");
			break;
		case 12:
			System.out.print("Dezember");
			break;
		default:
			System.out.print("Geben Sie eine g�ltige Zahl von 1 bis 12 ein.");
		}
		
		sc.close();
	}

}
