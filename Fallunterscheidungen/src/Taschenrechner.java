import java.util.Scanner;
import java.text.DecimalFormat;
public class Taschenrechner {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		DecimalFormat df = new DecimalFormat("#0.00");
		System.out.print("Geben Sie eine Zahl ein: ");
		double a = sc.nextDouble();
		System.out.print("Geben Sie noch eine Zahl ein: ");
		double b = sc.nextDouble();
		double c;
		System.out.print("Welche Rechenoperation wollen Sie w�hlen? (+,-,*,/): ");
		String operation = sc.next();
		switch (operation) {
		case "+":
			c = a + b;
			System.out.print("Ergebnis: " +df.format(c));
			break;
		case "-":
			c = a - b;
			System.out.print("Ergebnis: " +df.format(c));
			break;
		case "*":
			c = a * b;
			System.out.print("Ergebnis: " +df.format(c));
			break;
		case "/":
			c = a / b;
			System.out.print("Ergebnis: " +df.format(c));
			break;
		default:
			System.out.print("Geben Sie eine g�ltige Rechenoperation ein (+,-,*,/).");
		}
		sc.close();
	}

}
