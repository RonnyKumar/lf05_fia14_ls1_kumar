import java.util.Scanner;
public class Nutzer_Abfrage {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie ihren Namen ein: ");
		String name = sc.next(); 
		
		System.out.print("Bitte geben Sie ihr Alter ein: ");
		int alter = sc.nextInt();

		System.out.print("Ihr Name lautet: " + name);
		System.out.print("\nIhr Alter ist: " + alter);
		
		sc.close();
		
	}

}
