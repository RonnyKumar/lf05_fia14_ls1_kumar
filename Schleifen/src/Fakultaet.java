import java.util.Scanner;
public class Fakultaet {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Geben Sie bitte eine nat�rliche Zahl zwischen 1 und 20 ein: ");
		int n = sc.nextInt();
		int f = 1;
		int k = 1;
		if (0 <= n && n <= 20) {
			if (n == 0) {
				System.out.print("Die Fakult�t von 0 ist 1.");
			} else {
				while (k <= n) {
					f = f * k;
					k++;
				}
				System.out.print(f);
			}
		} else {
			System.out.print("Die Zahl liegt nicht im Bereich von 1 bis 20.");
		}
		sc.close();
	}

}
