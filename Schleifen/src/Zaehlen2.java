import java.util.Scanner;
public class Zaehlen2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Bitte geben Sie eine natürliche Zahl ein: ");
		int n = sc.nextInt();
		int a = 0;
		int b = n;
		while (n > 0) {
			a++;
			System.out.print(a + " ");
			n--;
		}
		System.out.println(" ");
		while (b > 0) {
			System.out.print(b + " ");
			b--;
		}
		sc.close();
	}

}
