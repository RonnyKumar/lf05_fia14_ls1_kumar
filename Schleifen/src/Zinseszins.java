import java.util.Scanner;
import java.text.DecimalFormat;
public class Zinseszins {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		DecimalFormat df = new DecimalFormat("#0.00");
		System.out.print("Laufzeit (in Jahren) des Sparvertrags: ");
		int j = sc.nextInt();
		System.out.print("Wie viel Kapital (in Euro) m�chten Sie anlegen: ");
		double ek = sc.nextDouble();
		System.out.print("Zinssatz: ");
		double z = sc.nextDouble();
		z = (z / 100) + 1;
		double ak = ek;
		while (j > 0) {
			ek = ek * z;
			j--;
		}
		System.out.println("Eingezahltes Kapital: " + df.format(ak) + " Euro");
		System.out.print("Ausgezahltes Kapital: " + df.format(ek) + " Euro");
		sc.close();
	}

}
