import java.util.Scanner;
public class Quersumme {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Geben Sie bitte eine Zahl ein: ");
		int z = sc.nextInt();
		int q = 0;
		while (z > 0) {
			q = q + (z % 10);
			z = z / 10;
		}
		System.out.print("Die Quersumme betr�gt: " + q);
		sc.close();
	}

}
