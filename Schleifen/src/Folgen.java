
public class Folgen {

	public static void main(String[] args) {
		int v = 99;
		while (v >= 9) {
			System.out.print(v + " ");
			v = v - 3;
		}
		System.out.println(" ");
		int w = 1;
		int a = 3;
		while (w <= 400) {
			System.out.print(w + " ");
			w = w + a;
			a = a + 2;
		}
		System.out.println(" ");
		int x = 2;
		while (x <= 102) {
			System.out.print(x + " ");
			x = x + 4;
		}
		System.out.println(" ");
		int y = 4;
		int b = 12;
		while (y <= 1024) {
			System.out.print(y + " ");
			y = y + b;
			b = b + 8;
		}
		System.out.println(" ");
		int z = 2;
		while (z <= 32768) {
			System.out.print(z + " ");
			z = z * 2;
		}
	}

}
