import java.util.Scanner;
public class Summe {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Bitte geben Sie einen begrenzenden Wert ein: ");
		int n = sc.nextInt();
		int a = 0;
		int x = A(n ,a);
		System.out.println("Ergebnis f�r A): " + x);
		int b = 0;
		int y = B(n, b);
		System.out.println("Ergebnis f�r B): " + y);
		int c = 0;
		int z = C(n, c);
		System.out.println("Ergebnis f�r C): " + z);
		sc.close();
	}
	
	public static int A(int n, int a) {
		while (n > 0) {
			a = a + n;
			n--;
		}
		return a;
	}
	
	public static int B(int n, int b) {
		while (n > 0) {
			b = b + (2 * n);
			n--;
		}
		return b;
	}
	
	public static int C(int n, int c) {
		while (n > 0) {
			c = c + (2 * n - 1);
			n--;
		}
		return c;
	}
	
}
