
public class Kistenklasse {
	private double hoehe;
	private double breite;
	private double tiefe;
	private String farbe;


public Kistenklasse(double hoehe, double breite, double tiefe, String farbe) {
	this.hoehe = hoehe;
	this.breite = breite;
	this.tiefe = tiefe; 
	this.farbe = farbe;
}

public double getVolumen() {
	double volumen = hoehe * breite * tiefe;
	return volumen;
}

public String getFarbe() {
	return farbe;
}

public void setFarbe(String farbe) {
	this.farbe = farbe;
}

}
