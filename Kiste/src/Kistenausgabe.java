
public class Kistenausgabe {

	public static void main(String[] args) {
		
		Kistenklasse test = new Kistenklasse(10, 10, 10, "Rot");
		
		System.out.println(test.getFarbe());
		System.out.println(test.getVolumen());
		
		System.out.println(); 
		
		Kistenklasse kiste = new Kistenklasse(0, 0, 0, "Blau");
		
		System.out.println(kiste.getFarbe());
		System.out.println(kiste.getVolumen());
		
		System.out.println();
		
		Kistenklasse abc = new Kistenklasse(20, 45, 66, "Gr�n");
		
		System.out.println(abc.getFarbe());
		System.out.println(abc.getVolumen());
		
	}

}
