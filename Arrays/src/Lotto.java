import java.util.Arrays;
import java.util.Scanner;

public class Lotto {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String[] lotto = {"3", "7", "12", "18", "37", "42"};
		for (int i = 0; i < lotto.length; i++) {
			System.out.print(lotto[i] + " ");
		}
		System.out.println(" ");
		System.out.print("Geben Sie eine Zahl zur Überprüfung ein: ");
		String a = sc.next();
		
		if(Arrays.asList(lotto).contains(a) == true) {
			System.out.print("Die Zahl " + a + " ist in der Ziehung enthalten.");
		} else {
			System.out.print("Die Zahl " + a + " ist nicht in der Ziehung enthalten.");
		}
		sc.close();
	}

}
